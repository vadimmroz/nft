import React, {useEffect, useState} from 'react';
import classes from "./header.module.css"

const Header = ({handleRight, isRightVisible}) => {
    const [Eth, setEth] = useState(3.25);
    useEffect(() => {
        setInterval(() => {
            let a = Math.random() * (0.02 - (0)) + (0)
            if (Eth >= a) {
                setEth(Math.random() > 0.5 ? Eth + a : Eth - a)
            }
        }, 15000)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    return (
        <header className={classes.header}>
            <input type="text" placeholder="🔍      Search by creator or collection"/>
            <nav style={isRightVisible ? {"marginRight": "auto"} : {}}>
                <button><img src="IconETH.svg" alt=""/> {Eth.toFixed(2)} ETH</button>
                <button><img src="IconNotification.svg" alt=""/></button>
                <button>Create</button>
                <button>Connect Wallet</button>
            </nav>
            <img src="unsplash_pmACe385Ruo.png" alt="avatar"/>
            <div>
                <h2>Musfiqur Rahman</h2>
                <p>ryzenpixel@gmail.com</p>
            </div>
            <div className={classes.arrow} onClick={handleRight}><img src="arrow.svg" alt="arrow" /></div>
        </header>
    );
};

export default Header;