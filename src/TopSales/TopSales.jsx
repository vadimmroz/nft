import React from 'react';
import classes from "./topSales.module.css";

const TopSales = () => {
    const data = [
        {
            id: 0,
            name: 'dicar',
            coast: 232102,
            verefy: true,
            img: 'https://i.ibb.co/C8ywTTt/Rectangle-1400-5.png'
        },
        {
            id: 1,
            name: 'astroo2',
            coast: 172023,
            verefy: true,
            img: 'https://i.ibb.co/g4Pg67X/Rectangle-1400-2.png'
        }, {
            id: 2,
            name: 'micle',
            coast: 92002,
            verefy: false,
            img: 'https://i.ibb.co/dmCLR5q/Rectangle-1400-1.png'
        }, {
            id: 3,
            name: '11erorD.',
            coast: 88024,
            verefy: false,
            img: 'https://i.ibb.co/g7ypYf0/Rectangle-1400.png'
        }, {
            id: 4,
            name: 'astroo2',
            coast: 172023,
            verefy: true,
            img: 'https://i.ibb.co/WP8ptJ1/Rectangle-1400-3.png'
        }, {
            id: 5,
            name: 'astroo2',
            coast: 172023,
            verefy: true,
            img: 'https://i.ibb.co/vPLDSTL/Rectangle-1400-4.png'
        },
        {
            id: 6,
            name: 'dicar',
            coast: 232102,
            verefy: true,
            img: 'https://i.ibb.co/C8ywTTt/Rectangle-1400-5.png'
        },
        {
            id: 7,
            name: 'astroo2',
            coast: 172023,
            verefy: true,
            img: 'https://i.ibb.co/g4Pg67X/Rectangle-1400-2.png'
        }, {
            id: 8,
            name: 'micle',
            coast: 92002,
            verefy: false,
            img: 'https://i.ibb.co/dmCLR5q/Rectangle-1400-1.png'
        }, {
            id: 9,
            name: '11erorD.',
            coast: 88024,
            verefy: false,
            img: 'https://i.ibb.co/g7ypYf0/Rectangle-1400.png'
        }, {
            id: 10,
            name: 'astroo2',
            coast: 172023,
            verefy: true,
            img: 'https://i.ibb.co/WP8ptJ1/Rectangle-1400-3.png'
        }, {
            id: 11,
            name: 'astroo2',
            coast: 172023,
            verefy: true,
            img: 'https://i.ibb.co/vPLDSTL/Rectangle-1400-4.png'
        }


    ]
    return (
        <section className={classes.topSales}>
            <h3>
                ⭐ Top Sellers
            </h3>
            <div className={classes.slider}>
                {data.map(e => {
                    return (<div className={classes.item} key={e.id}>
                        <img src={e.img} alt=""/>
                        <div>
                            <h4>@{e.name}</h4>
                            <p>${e.coast}</p>
                        </div>
                    </div>)
                })}
            </div>
        </section>
    );
};

export default TopSales;