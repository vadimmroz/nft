import React from 'react';
import classes from "./popular.module.css"
import Item from "./components/Item/Item";


const Popular = () => {
    const data = [
        {
            img: "https://i.ibb.co/WHr2yZP/image-341.png",
            price: 17.53,
            time: Date.now() + Math.random() * 86400000,
            id: 0
        },
        {
            img: "https://i.ibb.co/df94q5K/unsplash-Sm-CKTIc-H5-E-1.png",
            price: 17.53,
            time: Date.now() + Math.random() * 86400000,
            id: 1
        }
    ]
    return (
        <section className={classes.popular}>
            <div className={classes.popularText}>
                <h2>Popular NFT’s Live Auction</h2>
                <a href="/home">Show More <img src="arrow2.svg" alt=""/></a>
            </div>
            <div className={classes.slider}>
                {data.map((e) => <Item key={e.id} item={e}/>)}
            </div>
        </section>
    );
};

export default Popular;