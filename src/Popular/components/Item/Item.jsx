import React, {useEffect, useState} from 'react';
import classes from "./item.module.css"


const Item = ({item}) => {
    const [time, setTime] = useState(item.time)
    useEffect(() => {
        setTimeout(() => {
            let a = time - 1000
            setTime(a)
        }, 1000)

    }, [time])

    return (
        <div className={classes.item} style={{backgroundImage: 'url("' + item.img + ')"'}}>
            <div>
                <div className={classes.firstText}>
                    <h3>
                        {
                            (time > 0 ? Math.floor(time / 1000 / 60 / 60) % 24 : 0) > 9 ? (time > 0 ? Math.floor(time / 1000 / 60 / 60) % 24 : 0) : ("0" + (time > 0 ? Math.floor(time / 1000 / 60 / 60) % 24 : 0))
                        }h
                        :
                        {
                            (time > 0 ? Math.floor(time / 1000 / 60) % 60 : 0) > 9 ? (time > 0 ? Math.floor(time / 1000 / 60) % 60 : 0) : ("0" + (time > 0 ? Math.floor(time / 1000 / 60) % 60 : 0))
                        }m
                        :
                        {
                            (time > 0 ? Math.floor(time / 1000) % 60 : 0) > 9 ? (time > 0 ? Math.floor(time / 1000) % 60 : 0) : ("0" + (time > 0 ? Math.floor(time / 1000) % 60 : 0))
                        }s

                    </h3>
                    <h3>
                        17.53 ETH
                    </h3>
                </div>
                <div className={classes.secondText}>
                    <p>
                        Time Remaining
                    </p>
                    <p>
                        Highest Bid
                    </p>
                </div>
                <button>
                    Place A Bid
                </button>
            </div>
        </div>
    );
};

export default Item;