
import './App.css';
import {Navigate, Route, Routes} from "react-router-dom";
import Home from "./Home/Home";

function App() {
    return (
        <Routes>
            <Route path="*" element={<Navigate to="home"/>}/>
            <Route path="home" element={<Home/>}/>
            <Route path="buy" element={<Home/>}/>
            <Route path="message" element={<Home/>}/>
            <Route path="activity" element={<Home/>}/>
            <Route path="time" element={<Home/>}/>
            <Route path="wallet" element={<Home/>}/>
            <Route path="friends" element={<Home/>}/>
            <Route path="settings" element={<Home/>}/>
            <Route path="logOut" element={<Home/>}/>
        </Routes>
    );
}

export default App;
