import React, {useEffect, useState} from 'react';
import classes from './hot.module.css'

const Hot = () => {
    const [data, setData] = useState([
        {
            id: 0,
            time: new Date(2023, 4, 17, 11, 1, 10) - new Date(),
            likes: 232,
            isLike: false,
            name: 'Stretch Of Time',
            coast: 0.045,
            img: 'https://i.ibb.co/XZXMzpJ/Image.png'
        },
        {
            id: 1,
            time: new Date(2023, 4, 17, 17, 31, 5) - new Date(),
            likes: 100,
            isLike: true,
            name: 'Arcade Land',
            coast: 0.090,
            img: 'https://i.ibb.co/7g4V9c1/Image-4.png'
        },
        {
            id: 2,
            time: new Date(2023, 4, 17, 1, 52, 8) - new Date(),
            likes: 456,
            isLike: false,
            name: 'Shinsekai Portal',
            coast: 0.134,
            img: 'https://i.ibb.co/0XhbdW1/Image.png'
        }, {
            id: 3,
            time: new Date(2023, 4, 17, 19, 12, 45) - new Date(),
            likes: 723,
            isLike: true,
            name: 'Paper Cut',
            coast: 0.025,
            img: 'https://i.ibb.co/MScpY7v/Image-1.png'
        }, {
            id: 4,
            time: new Date(2023, 4, 17, 9, 43, 19) - new Date(),
            likes: 1098,
            isLike: false,
            name: 'Cyber Brokers',
            coast: 0.0345,
            img: 'https://i.ibb.co/k3Y9BXn/Art.png'
        }, {
            id: 5,
            time: new Date(2023, 4, 17, 4, 9, 11) - new Date(),
            likes: 389,
            isLike: false,
            name: 'Akuma Origins',
            coast: 0.045,
            img: 'https://i.ibb.co/5MLCsFB/Image-3.png'
        },
        {
            id: 6,
            time: new Date(2023, 4, 17, 11, 1, 10) - new Date(),
            likes: 232,
            isLike: false,
            name: 'Stretch Of Time',
            coast: 0.045,
            img: 'https://i.ibb.co/XZXMzpJ/Image.png'
        },
        {
            id: 7,
            time: new Date(2023, 4, 17, 17, 31, 5) - new Date(),
            likes: 100,
            isLike: true,
            name: 'Arcade Land',
            coast: 0.090,
            img: 'https://i.ibb.co/7g4V9c1/Image-4.png'
        },
        {
            id: 8,
            time: new Date(2023, 4, 17, 1, 52, 8) - new Date(),
            likes: 456,
            isLike: false,
            name: 'Shinsekai Portal',
            coast: 0.134,
            img: 'https://i.ibb.co/0XhbdW1/Image.png'
        }, {
            id: 9,
            time: new Date(2023, 4, 17, 19, 12, 45) - new Date(),
            likes: 723,
            isLike: true,
            name: 'Paper Cut',
            coast: 0.025,
            img: 'https://i.ibb.co/MScpY7v/Image-1.png'
        }, {
            id: 10,
            time: new Date(2023, 4, 17, 9, 43, 19) - new Date(),
            likes: 1098,
            isLike: false,
            name: 'Cyber Brokers',
            coast: 0.0345,
            img: 'https://i.ibb.co/k3Y9BXn/Art.png'
        }, {
            id: 11,
            time: new Date(2023, 4, 17, 4, 9, 11) - new Date(),
            likes: 389,
            isLike: false,
            name: 'Akuma Origins',
            coast: 0.045,
            img: 'https://i.ibb.co/5MLCsFB/Image-3.png'
        }


    ])
    const handleLike = (e) => {
        let a = data
        a[e].isLike = !a[e].isLike
        setData(a)
    }

    useEffect(() => {
        let timer
        clearInterval(timer)
        timer = setInterval(() => {
            data.forEach(e => {
                let a = e
                a.time = a.time - 1000
                setData([...data.slice(0, a.id), a, ...data.slice(a.id, data.length - 1)])
            })
        }, 1000)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <section className={classes.hot}>
            <h4>
                🔥 Hot Bids
            </h4>
            <div className={classes.slider}>
                {data.map((e) => {
                    const hours = e.time > 0 ? Math.floor(e.time / 1000 / 60 / 60) % 24 : 0;
                    const minutes = e.time > 0 ? Math.floor(e.time / 1000 / 60) % 60 : 0;
                    const seconds = e.time > 0 ? Math.floor(e.time / 1000) % 60 : 0;

                    return (
                        <div key={e.id} className={classes.item}>
                            <div style={{display:"flex", flexDirection:"column"}}>
                                <div className={classes.header}>
                                    <div className={classes.time}>
                                        <p>{
                                            (hours > 9 ? hours : ("0" + hours)) + ":" + (minutes > 9 ? minutes : ("0" + minutes)) + ":" + (seconds > 9 ? seconds : ("0" + seconds))
                                        }</p>
                                    </div>
                                    <p>{e.likes}</p>
                                    <img onClick={() => handleLike(e.id)} src={e.isLike ? "like.svg" : "like1.svg"}
                                         alt=""/>
                                </div>
                                <img src={e.img} alt=""/>
                                <h5>
                                    {e.name}
                                </h5>
                                <p>
                                    {e.coast} ETH
                                </p>
                            </div>
                            <div>

                            </div>
                        </div>
                    )
                })}
            </div>
        </section>
    );
};

export default Hot;