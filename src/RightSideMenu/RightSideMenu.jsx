import React from 'react';
import classes from "./rightSideMenu.module.css"
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import classNames from "classnames";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

const RightSideMenu = ({handleRight, anim}) => {
    return (
        <div className={classNames(classes.rightMenu, anim=== "remove" ? classes.remove : classes.add)}>
            <div className={classes.closed} onClick={handleRight}>
                <img src="reject.svg" alt=""/>
            </div>
            <img src="Avatar.png" alt="avatar"/>
            <p className={classes.name}>Musfiqur Rahman</p>
            <div className={classes.perks}>
                <div>
                    <div>
                        <p>
                            120
                        </p>
                    </div>
                    <p>
                        Asset
                    </p>
                </div>
                <div>
                    <div>
                        <p>
                            10K
                        </p>
                    </div>
                    <p>
                        Followers
                    </p>
                </div>
                <div>
                    <div>
                        <p>
                            70k
                        </p>
                    </div>
                    <p>
                        Likes
                    </p>
                </div>
                <div>
                    <div>
                        <p>
                            60
                        </p>
                    </div>
                    <p>
                        Bidding
                    </p>
                </div>
            </div>
            <h4>Your Balance</h4>
            <button>
                <img src="IconEthereum.svg" alt=""/>
                <p>4,668 ETH</p>
                <span>Add ></span>
            </button>
            <div className={classes.graph}>
                <h5>
                    Revenue
                </h5>
                <p>
                    This Month v
                </p>

            </div>
            <div className={classes.bar}>
                <img src="Graph.svg" alt=""/>
            </div>
            <div className={classes.activity}>
                <div className={classes.text}>
                    <h5>Recent Activity</h5>
                    <p>See All -></p>
                </div>
                <div className={classes.item}>
                    <img src="cristal.png" alt=""/>
                    <div className={classes.text}>
                        <h6>
                            Crystal_Art
                        </h6>
                        <p>
                            by
                            <span>
                                 @rudepixxel
                            </span>
                        </p>
                    </div>
                    <div className={classes.coast}>
                        <p>
                            New Bid
                        </p>
                        <h5>
                            6.25 ETH
                        </h5>
                        <p>
                            3m ago
                        </p>
                    </div>
                </div>
                <div className={classes.item}>
                    <img src="creative.png" alt=""/>
                    <div className={classes.text}>
                        <h6>
                            Creative Art
                        </h6>
                        <p>
                            by
                            <span>
                                @songkang
                            </span>
                        </p>
                    </div>
                    <div className={classes.coast}>
                        <p>
                            New Bid
                        </p>
                        <h5>
                            7.50 ETH
                        </h5>
                        <p>
                            3m ago
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RightSideMenu;