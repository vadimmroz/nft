import React, {useState} from 'react';
import SideMenu from "../SideMenu/SideMenu";
import Header from "../Header/Header";
import Popular from "../Popular/Popular";
import RightSideMenu from "../RightSideMenu/RightSideMenu";
import TopSales from "../TopSales/TopSales";
import Hot from "../Hot/Hot";

const Home = () => {
    const [isRightVisible, setIsRightVisible] = useState(false)
    const [anim, setAnim] = useState("remove")
    const handleRight = () => {

        if(anim === "remove"){
            setAnim("add")
            setIsRightVisible(!isRightVisible)
        }else{
            setAnim("remove")
            setTimeout(()=>{
                setIsRightVisible(!isRightVisible)
            }, 300)

        }
    }
    return (
        <>
            <SideMenu/>
            <Header handleRight={handleRight} isRightVisible={isRightVisible}/>
            <Popular/>
            {isRightVisible && <RightSideMenu handleRight={handleRight} anim={anim} />}
            <TopSales/>
            <Hot/>
        </>
    );
};

export default Home;