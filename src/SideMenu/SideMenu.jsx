import React from 'react';
import classes from "./sideMenu.module.css"
import {NavLink} from "react-router-dom";

const SideMenu = () => {
    return (
        <div className={classes.sideMenu}>
            <img src="logo.svg" alt="logo"/>
            <nav>
                <NavLink to='/home'><img src="IconHome.svg" alt=""/></NavLink>
                <NavLink to='/buy'><img src="IconBuy.svg" alt=""/></NavLink>
                <NavLink to='/message'><img src="IconMessage.svg" alt=""/></NavLink>
                <NavLink to='/activity'><img src="IconActivity.svg" alt=""/></NavLink>
                <NavLink to='/time'><img src="IconTimeCircle.svg" alt=""/></NavLink>
            </nav>
            <nav>
                <NavLink to='/wallet'><img src="IconWallet.svg" alt=""/></NavLink>
                <NavLink to='/friends'><img src="IconFriends.svg" alt=""/></NavLink>
                <NavLink to='/settings'><img src="IconSettings.svg" alt=""/></NavLink>
            </nav>
            <NavLink to='/logOut'><img src="IconLogOut.svg" alt=""/></NavLink>
        </div>
    );
};

export default SideMenu;